# storage/nvdimm/fsdax_attributes_write_cache

Storage: nvdimm feature test for device cache turn off/on test BZ1457556

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
